<?php

namespace App\Service;

use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV1;

class UuidService
{
    public static function generate(): UuidV1
    {
        return Uuid::v1();
    }
}