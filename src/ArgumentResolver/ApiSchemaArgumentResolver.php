<?php

declare(strict_types=1);

namespace App\ArgumentResolver;

use App\Model\ApiSchemaInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class ApiSchemaArgumentResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        private readonly SerializerInterface $serializer,
    ) {}

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        $content = $request->getContent();

        $model = $this->serializer->deserialize($content, $argument->getType(), JsonEncoder::FORMAT);

        yield $model;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        try {
            $reflection = new \ReflectionClass($argument->getType());
            if ($reflection->implementsInterface(ApiSchemaInterface::class)) {
                return true;
            }

            return false;
        } catch (\ReflectionException $exception) {
            return false;
        }
    }
}
