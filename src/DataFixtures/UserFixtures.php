<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        //create user admin
        $user = new User('Admin');

        $encoded = $this->passwordHasher->hashPassword($user, 'password');
        $user
            ->setRoles([User::ROLE_ADMIN])
            ->setPassword($encoded)
        ;

        $manager->persist($user);

        //create user
        for ($i = 1; $i <= 5; $i++) {
            $user = new User("login.$i");

            $encoded = $this->passwordHasher->hashPassword($user, 'password');
            $user->setPassword($encoded);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
