<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use App\Service\UuidService;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\UuidV1;

#[ORM\Table(name: 'products')]
#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product extends BaseEntity
{
    #[Groups(['create_product', 'update_product', 'create_order'])]
    #[ORM\Id]
    #[ORM\Column(type: "uuid", unique: true)]
    private UuidV1  $id;

    #[Groups(['create_product', 'update_product'])]
    #[ORM\Column(length: 255)]
    private string $name;

    #[Groups(['create_product', 'update_product'])]
    #[ORM\Column(length: 255)]
    private float $price;

    #[Groups(['create_product', 'update_product'])]
    #[ORM\ManyToOne(inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: false)]
    private Category $category;

    public function __construct(string $name, int $price, Category $category)
    {
        $this->id = UuidService::generate();
        $this->name = $name;
        $this->price = $price;
        $this->category = $category;
    }

    public function getId(): UuidV1
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
