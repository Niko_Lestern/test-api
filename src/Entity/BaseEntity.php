<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\HasLifecycleCallbacks]
#[ORM\MappedSuperclass]
class BaseEntity
{
    #[Groups(['added_at'])]
    #[ORM\Column]
    private DateTimeImmutable $addedAt;

    #[Groups(['update_at'])]
    #[ORM\Column]
    private DateTimeImmutable $updateAt;

    public function getAddedAt(): DateTimeImmutable
    {
        return $this->addedAt;
    }

    #[ORM\PrePersist]
    public function onPrePersist(): self
    {
        $this->addedAt = new DateTimeImmutable;
        $this->updateAt = new DateTimeImmutable;

        return $this;
    }

    public function getUpdateAt(): DateTimeImmutable
    {
        return $this->updateAt;
    }

    #[ORM\PreUpdate]
    public function onPreUpdate(): self
    {
        $this->updateAt = new DateTimeImmutable;

        return $this;
    }
}
