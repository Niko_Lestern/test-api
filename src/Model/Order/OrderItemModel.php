<?php

namespace App\Model\Order;

use App\Model\ApiSchemaInterface;
use Symfony\Component\Uid\UuidV1;
use Symfony\Component\Validator\Constraints as Assert;

class OrderItemModel implements ApiSchemaInterface
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public UuidV1 $id;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Positive(message: 'not correct value')]
    public int $quantity;
}