<?php

namespace App\Model\Order;

use App\Model\ApiSchemaInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CreateOrderModel implements ApiSchemaInterface
{
    /**
    * @var OrderItemModel[]
    */
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public array $items;
}