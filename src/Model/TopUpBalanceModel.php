<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class TopUpBalanceModel implements ApiSchemaInterface
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Positive(message: 'not correct value')]
    public float $balance;
}