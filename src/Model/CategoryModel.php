<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class CategoryModel implements ApiSchemaInterface
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $name;
}