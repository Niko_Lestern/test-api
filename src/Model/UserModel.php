<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class UserModel implements ApiSchemaInterface
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $login;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(
        min: 6,
        max: 64,
        minMessage: 'Your password must be at least {{ limit }} characters long',
        maxMessage: 'Your password cannot be longer than {{ limit }} characters',
    )]
    public ?string $plainPassword;
}