<?php

namespace App\Model;

use Symfony\Component\Uid\UuidV1;
use Symfony\Component\Validator\Constraints as Assert;

class ProductModel implements ApiSchemaInterface
{
    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?string $name;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    public ?int $price;

    #[Assert\NotBlank]
    #[Assert\NotNull]
    public UuidV1 $category;
}