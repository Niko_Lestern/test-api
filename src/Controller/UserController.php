<?php

namespace App\Controller;

use App\Model\TopUpBalanceModel;
use App\Model\UserModel;

use App\Manager\User\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    #[Route('/api/users', name: 'create_user', methods: 'POST')]
    public function createUser(UserModel $model, UserManager $userManager, ValidatorInterface $validator): JsonResponse
    {
        $errors = $validator->validate($model);

        if (count($errors) > 0) {
             return $this->json($errors);
        }

        $user = $userManager->create($model);

        return $this->json(
            $user,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['create_user', 'added_at']],
        );
    }
    #[Route('/api/balance/replenish', name: 'top_up_balance', methods: 'POST')]
    #[IsGranted('ROLE_USER')]
    public function topUpBalance(TopUpBalanceModel $model, UserManager $manager, ValidatorInterface $validator): JsonResponse
    {
        $errors = $validator->validate($model);

        if (count($errors) > 0) {
            return $this->json($errors);
        }

        $user = $manager->topUpBalance($model);

        return $this->json(
            $user,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['get_user', 'update_at']],
        );
    }
}
