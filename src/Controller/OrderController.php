<?php

namespace App\Controller;

use App\Manager\Order\OrderManager;
use App\Model\Order\CreateOrderModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderController extends AbstractController
{
    #[Route('/api/order', name: 'app_order', methods: 'POST')]
    #[IsGranted('ROLE_USER')]
    public function createOrder(CreateOrderModel $model, OrderManager $orderManager, ValidatorInterface $validator): JsonResponse
    {
        $errors = $validator->validate($model);

        if (count($errors) > 0) {
            return $this->json($errors);
        }
        $order = $orderManager->create($model);

        return $this->json(
            $order,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['create_order', 'added_at']],
        );
    }
}
