<?php

namespace App\Controller;


use App\Manager\Category\CategoryManager;
use App\Model\CategoryModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\UuidV1;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryController extends AbstractController
{
    #[Route('/api/category', name: 'create_category', methods: 'POST')]
    #[IsGranted('ROLE_ADMIN')]
    public function createCategory(CategoryModel $model, CategoryManager $categoryManager, ValidatorInterface $validator): JsonResponse
    {
        $errors = $validator->validate($model);

        if (count($errors) > 0) {
            return $this->json($errors);
        }

        $category = $categoryManager->create($model);

        return $this->json(
            $category,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['create_category', 'added_at']],
        );
    }

    #[Route('/api/category/{id}', name: 'update_category', methods: 'PUT')]
    #[IsGranted('ROLE_ADMIN')]
    public function updateCategory(UuidV1 $id, CategoryModel $model, CategoryManager $categoryManager, ValidatorInterface $validator): JsonResponse
    {
        $errors = $validator->validate($model);

        if (count($errors) > 0) {
            return $this->json($errors);
        }

        $category = $categoryManager->update($model, $id);

        return $this->json(
            $category,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['update_category', 'update_at']],
        );
    }
}
