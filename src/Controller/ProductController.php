<?php

namespace App\Controller;

use App\Manager\Product\ProductManager;
use App\Model\ProductModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\UuidV1;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductController extends AbstractController
{
    #[Route('/api/product', name: 'create_product', methods: 'POST')]
    #[IsGranted('ROLE_ADMIN')]
    public function createProduct(ProductModel $model, ProductManager $productManager, ValidatorInterface $validator): JsonResponse
    {
        $errors = $validator->validate($model);

        if (count($errors) > 0) {
            return $this->json($errors);
        }

        $product = $productManager->create($model);

        return $this->json(
            $product,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['create_product', 'added_at']],
        );
    }

    #[Route('/api/product/{id}', name: 'update_product', methods: 'PUT')]
    #[IsGranted('ROLE_ADMIN')]
    public function updateProduct(UuidV1 $id, ProductModel $model, ProductManager $productManager, ValidatorInterface $validator): JsonResponse
    {
        $errors = $validator->validate($model);

        if (count($errors) > 0) {
            return $this->json($errors);
        }

        $product = $productManager->update($model, $id);

        return $this->json(
            $product,
            Response::HTTP_CREATED,
            [],
            ['groups' => ['update_product', 'update_at']],
        );
    }
}
