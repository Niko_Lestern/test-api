<?php

namespace App\Manager\Order;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Manager\Product\ProductManager;
use App\Manager\User\UserManager;
use App\Model\Order\CreateOrderModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class OrderManager
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ProductManager $productManager,
        private readonly Security $security,
        private readonly UserManager $userManager
    ) {}

    public function create(CreateOrderModel $model): Order
    {
        //calculation of the total price of the order
        $totalPrice = 0.0;
        foreach ($model->items as $item) {
            $product = $this->productManager->get($item->id);
            $totalPrice += $product->getPrice() * $item->quantity;
        }

        $this->userManager->isEnoughCashBuy($totalPrice);

        $this->userManager->debitingCashBalance($totalPrice);

        $order = new Order($this->security->getUser());
        foreach ($model->items as $item) {
            $product = $this->productManager->get($item->id);
            $order->addItem(new OrderItem($product, $item->quantity));
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $order;
    }

}