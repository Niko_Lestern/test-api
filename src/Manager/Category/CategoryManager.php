<?php

namespace App\Manager\Category;

use App\Entity\Category;
use App\Model\CategoryModel;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Uid\UuidV1;

class CategoryManager
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly CategoryRepository $categoryRepository
    ) {}

    public function create(CategoryModel $model): Category
    {
        $category = new Category($model->name);

        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $category;
    }

    public function update(CategoryModel $model, UuidV1 $id): Category
    {
        $category = $this->get($id);

        $category->setName($model->name);

        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $category;
    }

    public function get(UuidV1 $id): Category
    {
        $category = $this->categoryRepository->find($id);
        if (!$category instanceof Category) {
            throw new NotFoundHttpException('Category not found');
        }

        return $category;
    }

}