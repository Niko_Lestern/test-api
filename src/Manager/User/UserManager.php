<?php

namespace App\Manager\User;

use App\Entity\User;
use App\Model\TopUpBalanceModel;
use App\Model\UserModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly EntityManagerInterface      $entityManager,
        private readonly Security                    $security
    ) {}

    public function create(UserModel $model): UserInterface
    {
        $user = new User($model->login);

        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $model->plainPassword
        );

        $user->setPassword($hashedPassword);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function topUpBalance(TopUpBalanceModel $model): UserInterface
    {
        $user = $this->security->getUser();
        $user->setBalance($updatedBalance = $user->getBalance() + $model->balance);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function isEnoughCashBuy(float $total): void
    {
        $user = $this->security->getUser();
        if ($total > $user->getBalance()) {
            throw new BadRequestException("insufficient funds", Response::HTTP_BAD_REQUEST);
        }
    }

    public function debitingCashBalance(float $total): UserInterface
    {
        $user = $this->security->getUser();

        $user->setBalance($user->getBalance()-$total);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}