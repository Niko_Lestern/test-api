<?php

namespace App\Manager\Product;

use App\Entity\Product;
use App\Model\ProductModel;
use App\Manager\Category\CategoryManager;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Uid\UuidV1;

class ProductManager
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly CategoryManager $categoryManager,
        private readonly ProductRepository $productRepository
    ) {}

    public function create(ProductModel $model): Product
    {
        $category = $this->categoryManager->get($model->category);

        $product = new Product(
            $model->name,
            $model->price,
            $category
        );

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    public function update(ProductModel $model, UuidV1 $id): Product
    {
       $product = $this->get($id);
       $category = $this->categoryManager->get($model->category);

       $product
           ->setName($model->name)
           ->setPrice($model->price)
           ->setCategory($category)
       ;

       return $product;
    }

    public function get(UuidV1 $id): Product
    {
        $product = $this->productRepository->find($id);
        if (!$product instanceof Product) {
            throw new NotFoundHttpException('Product not found');
        }

        return $product;
    }
}