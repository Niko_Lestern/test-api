<?php

namespace App\Tests\Controller;

use App\Entity\Product;
use App\Entity\User;
use App\Service\UuidService;
use App\Tests\CustomApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProductTest extends CustomApiTestCase
{
    public function testCreateProductWrongCategory(): void
    {
        $admin = $this->createUser('Admin', 'password', User::ROLE_ADMIN);
        $authenticatedClient = $this->AuthenticateClient($admin->getLogin());

        $authenticatedClient->jsonRequest('POST', '/api/product', [
            "name" => "name",
            "price" => 10,
            "category" => $randomUuid = UuidService::generate()
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testCreateProduct(): void
    {
        //Created a product as an admin
        $admin = $this->createUser('Admin', 'password', User::ROLE_ADMIN);
        $authenticatedClient = $this->AuthenticateClient($admin->getLogin());

        $category = $this->createCategory();

        $authenticatedClient->jsonRequest('POST', '/api/product', [
            "name" => "name",
            "price" => 10,
            "category" => $category->getId()
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

        //checking whether a product has been created
        $data = $this->decodeJsonResponse();

        $category = $this->entityManager->getRepository(Product::class)->find($data['id']);
        $this->assertIsObject($category, 'the created product could not be found');
    }

    public function testUpdateProduct(): void
    {
        $category = $this->createCategory();
        $product = $this->createProduct($category);

        //Update a product as admin
        $admin = $this->createUser('Admin', 'password', User::ROLE_ADMIN);
        $authenticatedClient = $this->AuthenticateClient($admin->getLogin());

        $authenticatedClient->jsonRequest('PUT', '/api/product/' . $product->getId(), [
            "name" => "update name",
            "price" => 100,
            "category" => $category->getId()
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

        $updatedProduct = $this->entityManager->getRepository(Product::class)->find($product->getId());

        $this->assertNotEquals($product, $updatedProduct, 'the product has not been updated');

        //Test product for the update was not found
        $authenticatedClient->jsonRequest('PUT', '/api/product/9999', [
            "name" => "update name",
            "price" => 100,
            "category" => $category->getId()
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }
}