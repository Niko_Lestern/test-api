<?php

namespace App\Tests\Controller;

use App\Tests\CustomApiTestCase;

use Symfony\Component\HttpFoundation\Response;

class UserTest extends CustomApiTestCase
{
    public function testCreateUser(): void
    {
        $this->client->jsonRequest('POST', '/api/users', [
            "login" => "TestLogin",
            "plainPassword" => "TestPassword"
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }

    public function testLogIn(): void
    {
        $this->createUser();

        $this->client->jsonRequest('POST', '/api/login', [
            "login" => "InvalidLogin",
            "password" => "InvalidPassword"
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_UNAUTHORIZED);

        $this->client->jsonRequest('POST', '/api/login', [
            "login" => "login",
            "password" => "password"
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }
}
