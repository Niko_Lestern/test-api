<?php

namespace App\Tests\Controller;

use App\Entity\Category;
use App\Entity\User;
use App\Tests\CustomApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class CategoryTest extends CustomApiTestCase
{
    public function testCreateCategoryWithoutAccessRights(): void
    {
        $user = $this->createUser();
        $authenticatedClient = $this->AuthenticateClient($user->getLogin());

        $authenticatedClient->jsonRequest('POST', '/api/category', [
            "name" => "Category name"
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testCreateCategory(): void
    {
        //Create a product as an admin
        $admin = $this->createUser('Admin', 'password', User::ROLE_ADMIN);
        $authenticatedClient = $this->AuthenticateClient($admin->getLogin());

        $authenticatedClient->jsonRequest('POST', '/api/category', [
            "name" => "Category name"
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

        //checking whether a category has been created
        $data = $this->decodeJsonResponse();

        $category = $this->entityManager->getRepository(Category::class)->find($data['id']);
        $this->assertIsObject($category, 'the created category could not be found');

    }

    public function testUpdateCategory(): void
    {
        $category = $this->createCategory();

        //Update a category as admin
        $admin = $this->createUser('Admin', 'password', User::ROLE_ADMIN);
        $authenticatedClient = $this->AuthenticateClient($admin->getLogin());

        $authenticatedClient->jsonRequest('PUT', '/api/category/'. $category->getId(), [
            "name" => "updated name"
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);

        $updatedCategory = $this->entityManager->getRepository(Category::class)->find($category->getId());

        $this->assertNotEquals($category, $updatedCategory, 'the category has not been updated');

        //Test category for the update was not found
        $authenticatedClient->jsonRequest('PUT', '/api/category/9999', [
            "name" => "updated name"
        ]);

        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }
}