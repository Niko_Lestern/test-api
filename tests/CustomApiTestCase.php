<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomApiTestCase extends WebTestCase
{
    public const HOST = 'localhost:8000';

    protected EntityManagerInterface $entityManager;
    protected ContainerInterface $container;
    protected KernelBrowser $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient([], [
            'HTTP_HOST' => self::HOST,
        ]);
        $this->container = $this->client->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getManager();
    }
    protected function createUser(string $login = 'login', string $password = 'password',string $role = null): User
    {
        $user = new User($login);

        $encoded = $this->container->get(UserPasswordHasherInterface::class)
            ->hashPassword($user, $password);
        $user->setPassword($encoded);

        if ($role !== null) {
            $user->setRoles([$role]);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    //TODO заменить на DataFixture
    protected function createCategory(string $name = 'name'): Category
    {
        $category = new Category($name);

        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $category;
    }

    protected function createProduct(Category $category, string $name = 'name', $price = 10): Product
    {
        $product = new Product($name, $price, $category);

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    protected function AuthenticateClient($login = 'login', $password = 'password')
    {
        $this->client->jsonRequest('POST', '/api/login', [
            "login" => $login,
            "password" => $password
    ]);

        $data = $this->decodeJsonResponse();

        $this->client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $this->client;
    }

    protected function decodeJsonResponse() {
        return json_decode($this->client->getResponse()->getContent(), true);
    }
}